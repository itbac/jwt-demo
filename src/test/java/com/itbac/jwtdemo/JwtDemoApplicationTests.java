package com.itbac.jwtdemo;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.Base64Codec;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.nio.charset.StandardCharsets;
import java.util.Date;

@SpringBootTest
class JwtDemoApplicationTests {

	private static String salt = "itbac";

	@Test
	void createToken() {
		//当前时间 毫秒数
		long now = System.currentTimeMillis();
		//设置1小时后过期
		long exp = now + 60 * 60 * 1000;


		String token = Jwts.builder()
				//主键id
				.setId("777")
				//主题
				.setSubject("rose")
				//创建时间
				.setIssuedAt(new Date())
				//设置过期时间
				.setExpiration(new Date(exp))
				//加密算法、盐 , byte[]和 String 的盐也会不同。
				//RS256非对称加密算法
				.signWith(SignatureAlgorithm.RS256, salt)
				//自定义声明
				.claim("n","张三")
				.claim("s","OpenPlatform")
				//传map进去
//				.addClaims(map)
				//生成token
				.compact();

		System.out.println("token:" + token);
		String[] split = token.split("\\.");
		System.out.println(Base64Codec.BASE64.decodeToString(split[0]));
		System.out.println(Base64Codec.BASE64.decodeToString(split[1]));

/**
 * 相同的头部，可以固定起来，不传递给前端 eyJhbGciOiJIUzI1NiJ9
 * token:eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI4ODgiLCJzdWIiOiJyb3NlIiwiaWF0IjoxNjE2ODM0NDIxfQ.gl18X09BwR7XjlPaZNG0e5Ed_5pitYN_qBkJeJL-qbI
 *       eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI3NzciLCJzdWIiOiJyb3NlIiwiaWF0IjoxNjE2ODM0ODA1fQ.WayeOrRXRKdZSeO8xRlTNM40qim6j73xEqcWIvqP4Ho
 *       eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI3NzciLCJzdWIiOiJyb3NlIiwiaWF0IjoxNjE2ODM2NTE3LCJleHAiOjE2MTY4NDAxMTcsIm5hbWUiOiLlvKDkuIkiLCJzb3VyY2UiOiJPcGVuUGxhdGZvcm0ifQ.wG7dampOjcJgOzVtBSeOBusoMXk9EP-WcYREEyTCsSc
 *   加密算法
 * {"alg":"HS256"}
 * 主键，主题，创建时间，过期时间
 {"jti":"777","sub":"rose","iat":1616835854,"exp":1616839454}
 */
	}

	@Test
	public void parseToken(){
		String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJiYWMiLCJzY29wZSI6WyJhbGwiXSwibmFtZSI6IuW8oOS4iSIsImV4cCI6MTYxNjg4NDI1OCwiYXV0aG9yaXRpZXMiOlsiYWRtaW4iXSwianRpIjoiZmY4ZTFjMzEtZDA3OC00NzIzLWFjMGItMTUwNzk1YzVmYzAxIiwiY2xpZW50X2lkIjoiYWRtaW4ifQ.3pdgq6gDLkEcWf_7qPuN4G3BteU8_Oz5kPvPq6stERw";
				Claims claims = (Claims) Jwts.parser()
				.setSigningKey(salt.getBytes(StandardCharsets.UTF_8))
				.parse(token)
				.getBody();
		System.out.println(claims.getId());
		System.out.println(claims.getSubject());
//		获取自定义声明的数据
//		System.out.println(claims.get("n"));
//		System.out.println(claims.get("s"));

		String[] split = token.split("\\.");
		System.out.println(Base64Codec.BASE64.decodeToString(split[0]));
		System.out.println(Base64Codec.BASE64.decodeToString(split[1]));

	}
	@Test
	public void parseToken2(){

		String token = "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI3NzciLCJzdWIiOiJyb3NlIiwiaWF0IjoxNjE2ODk4NTUxLCJleHAiOjE2MTY5MDIxNTEsIm4iOiLlvKDkuIkiLCJzIjoiT3BlblBsYXRmb3JtIn0.2XhdKELGnSiJlXivgK9IFBhfdhAte62nBguVMtyMO8E";
		String extId = Jwts.parser().setSigningKey(salt).parseClaimsJws(token).getBody().getId();


	}

}
